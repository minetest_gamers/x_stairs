stairs.register_stair_and_slab(
	"magmacobble",
	"x_default:magmacobble",
	{cracky = 3},
	{"x_default_magmacobble.png"},
	"Magma Cobblestone Stair",
	"Magma Cobblestone Slab",
	default.node_sound_stone_defaults()
)

stairs.register_stair_and_slab(
	"snowcobble",
	"x_default:snowcobble",
	{choppy=1, wood=1, flammable=2},
	{"x_default_snowcobble.png"},
	"Snow Cobble Stair",
	"Snow Cobble Slab",
	default.node_sound_stone_defaults(),
	false
)

stairs.register_stair_and_slab(
	"icecobble",
	"x_default:icecobble",
	{choppy=1, wood=1, flammable=2},
	{"x_default_icecobble.png"},
	"Ice Cobble Stair",
	"Ice Cobble Slab",
	default.node_sound_stone_defaults(),
	false
)

stairs.register_stair_and_slab(
	"lapisblock",
	"x_default:lapisblock",
	{cracky = 2},
	{"x_default_lapis_block.png"},
	"Lapis Block Stair",
	"Lapis Block Slab",
	default.node_sound_stone_defaults(),
	true
)

stairs.register_stair_and_slab(
	"lapisbrick",
	"x_default:lapisbrick",
	{cracky = 2},
	{"x_default_lapis_brick.png"},
	"Lapis Brick Stair",
	"Lapis Brick Slab",
	default.node_sound_stone_defaults(),
	true
)

stairs.register_stair_and_slab(
	"lapiscobble",
	"x_default:lapiscobble",
	{cracky = 2},
	{"x_default_lapiscobble.png"},
	"Lapis Cobblestone Stair",
	"Lapis Cobblestone Slab",
	default.node_sound_stone_defaults(),
	true
)

stairs.register_stair_and_slab(
	"lapis_tile",
	"x_default:lapis_tile",
	{cracky = 2},
	{"x_default_lapis_tile.png"},
	"Lapis Tile Stair",
	"Lapis Tile Slab",
	default.node_sound_stone_defaults(),
	true
)

stairs.register_stair_and_slab(
	"lapis_gold_tile",
	"x_default:lapis_gold_tile",
	{cracky = 2},
	{"x_default_lapis_gold_tile.png"},
	"Lapis Gold Tile Stair",
	"Lapis Gold Tile Slab",
	default.node_sound_stone_defaults(),
	true
)

stairs.register_stair_and_slab(
	"lapis_gold_brick",
	"x_default:lapis_gold_brick",
	{cracky = 2},
	{"x_default_lapis_gold_brick.png"},
	"Lapis Gold Brick Stair",
	"Lapis Gold Brick Slab",
	default.node_sound_stone_defaults(),
	true
)

stairs.register_stair_and_slab(
	"lapis_gold_block",
	"x_default:lapis_gold_block",
	{cracky = 2},
	{"x_default_lapis_gold_block.png"},
	"Lapis Gold Block Stair",
	"Lapis Gold Block Slab",
	default.node_sound_stone_defaults(),
	true
)

stairs.register_stair_and_slab(
	"lapis_gold_obsidian",
	"x_default:lapis_gold_obsidian",
	{cracky = 2},
	{"x_default_lapis_gold_obsidian_side.png"},
	"Lapis Gold Obsidian Stair",
	"Lapis Gold Obsidian Slab",
	default.node_sound_stone_defaults(),
	true
)
